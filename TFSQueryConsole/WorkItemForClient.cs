﻿using Microsoft.TeamFoundation.WorkItemTracking.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TFSQueryConsole
{
    public class WorkItemForClient
    {
        public int? Id { get; set; }
        public string Title { get; set; }
        public string AssignedTo { get; set; }
        public string Description { get; set; }

        private char[] _separators = new char[] { '§', '-' };
        private List<string> _tags = new List<string>()
            {
                "</div>",
                "</span>",
                "</p>",
                "START",
            };
        public List<WorkItemsTimeRange> TimeRanges { get; set; }

        public WorkItemForClient(WorkItem workItem)
        {
            this.Id = workItem.Id;
            this.Title = workItem.Fields["System.Title"].ToString();
            this.AssignedTo = workItem.Fields["System.AssignedTo"].ToString();
            this.AssignedTo = this.AssignedTo.Remove(this.AssignedTo.IndexOf('<'));
            this.Description = workItem.Fields["System.Description"].ToString().ToUpper();
            this.TimeRanges = new List<WorkItemsTimeRange>();
            RecoverDateFromDescription();
        }
        public void RecoverDateFromDescription()
        {
            this.ManiuplateDescription();

            AddSeparator(this._tags[3]);            
            string[] temps = this.Description.Split(_separators[1],_separators[0]);
            int index = 0;
            foreach (string temp in temps)
            {
                if (temp.Contains("START:"))
                {
                    WorkItemsTimeRange innerTemp = new WorkItemsTimeRange();
                    innerTemp.DateFrom = SubStringDate(temp);
                    TimeRanges.Add(innerTemp);
                }
                if (temp.Contains("END:"))
                {
                    if(TimeRanges[index].DateFrom != null)
                    {
                        TimeRanges[index].DateTo = SubStringDate(temp);
                        index++;
                    }
                }
            }
                   
        }
        private DateTime? SubStringDate(string row)
        {
            string temp = Regex.Replace(row, @"[^0-9/]", "").Trim();
            DateTime? ret = null;
            try
            {
                ret = DateTime.Parse(temp);
                return ret;
            }
            catch (FormatException)
            {
                return ret;
            }
        }
        private void ManiuplateDescription()
        {
            foreach (string tag in _tags)
            {
                if (this.Description.Contains(tag))
                    AddSeparator(tag);
            }
            this.Description = Regex.Replace(this.Description, @"<[^>]+>|&NBSP;", "").Trim().Replace(" ", string.Empty).ToUpper();
            RemoveJunkFromDescription();
        }
        private  void RemoveJunkFromDescription()
        {
            string[] temp = this.Description.Split(_separators[0]);
            StringBuilder ret = new StringBuilder();
            foreach (string tempRow in temp)
            {
                if ((tempRow.Contains("START:") || tempRow.Contains("END:")) && tempRow.Contains('/'))
                {
                    ret.Append(tempRow);
                    ret.AppendLine();
                }                
            }
           this.Description = ret.ToString();
        }
        private void AddSeparator(string tag)
        {
            this.Description = this.Description.Replace(tag, _separators[0] + tag);
        }
    }
}
