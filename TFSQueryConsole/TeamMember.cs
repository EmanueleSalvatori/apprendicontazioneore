﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TFSQueryConsole
{
    public class TeamMember
    {
        public string Username { get; set; }
        public Dictionary<int, List<string>> ActivitiesByDay { get; set; }
        public Dictionary<int,List<int?>> IdActivitesByDay { get; set; }

        public TeamMember(List<WorkItemForClient> workItemForClients)
        {
            ActivitiesByDay = new Dictionary<int, List<string>>();
            IdActivitesByDay = new Dictionary<int, List<int?>>();
            this.Username = workItemForClients[0].AssignedTo;
            for(int i = 1; i <= 31; i++)
            {
                IdActivitesByDay.Add(i, new List<int?>());
                ActivitiesByDay.Add(i, new List<string>());              
                foreach (WorkItemForClient workItemForClient in workItemForClients)
                {
                    foreach (WorkItemsTimeRange timeRange in workItemForClient.TimeRanges)
                    { 
                        if (BetweenRanges(timeRange.DateFrom.HasValue?timeRange.DateFrom.Value.Day:-1, timeRange.DateTo.HasValue?timeRange.DateTo.Value.Day:timeRange.DateFrom.Value.Day,i))
                        {
                            ActivitiesByDay[i].Add(workItemForClient.Title);
                            IdActivitesByDay[i].Add(workItemForClient.Id);
                        }
                    }
                }
            }
           
        }
        private bool BetweenRanges(int a, int b, int number)
        {
            return (a <= number && number <= b);
        }

        private void ReportToCsv(int day, string activityTitle)
        {

          
        }
    }
}
