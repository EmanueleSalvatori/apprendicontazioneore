﻿using Microsoft.TeamFoundation.WorkItemTracking.WebApi;
using Microsoft.TeamFoundation.WorkItemTracking.WebApi.Models;
using Microsoft.VisualStudio.Services.Common;
using Microsoft.VisualStudio.Services.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using HtmlAgilityPack;
using System.IO;

namespace TFSQueryConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Guid teamfGUID = new Guid("4147b784-b47c-427c-89a9-83493e03bc87");
            Guid vaccaGUID = new Guid("3e31f219-e03a-47c1-bfce-ebb6d5f1e3a9");
            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(ValidateServerCertificate);
            VssConnection connection = new VssConnection(new Uri("https://itzek-tfs.fme.ads.fresenius.com/CVISCollection"), new VssBasicCredential("fme\vdesario", "d7jarre2cecty5bpwi2ce6n3qyenxiburbshc4nj6kwymzs6ploq"));
            WorkItemTrackingHttpClient witClient = connection.GetClient<WorkItemTrackingHttpClient>();

            WorkItemQueryResult res = witClient.QueryByIdAsync(vaccaGUID).Result;
            

            List<WorkItemForClient> works = new List<WorkItemForClient>();

            if (res.WorkItems.Any())
            {
                int skip = 0;
                const int batchSize = 100;
                IEnumerable<WorkItemReference> workItemRefs;

                do
                {
                    workItemRefs = res.WorkItems.Skip(skip).Take(batchSize);
                    if (workItemRefs.Any())
                    {
                        // get details for each work item in the batch
                        List<WorkItem> workItems = witClient.GetWorkItemsAsync(workItemRefs.Select(wir => wir.Id)).Result;


                        foreach (WorkItem workItem in workItems)
                        {
                            if (workItem.Fields.ContainsKey("System.Description") && workItem.Fields.ContainsKey("System.AssignedTo"))
                            {
                                WorkItemForClient workItemForClient = new WorkItemForClient(workItem);
                                works.Add(workItemForClient);
                            }
                            //definire logica per inserire in una lista di un nuovo oggetto da creare titolo task,nome e cognome persona,description
                        }
                    }
                    skip += batchSize;
                }
                while (workItemRefs.Count() == batchSize);
                //output nome e cognome persona; giorno; titolo task
            }
            else
            {
                Console.WriteLine("No work items were returned from query.");
            }
            List<TeamMember> teamList = TeamMembersFactory(new string[] { "Vacca" }, works);
           ;
            Console.WriteLine("Fine");


            //write csv persona;giorno;lista attività con separatore,
            using (StreamWriter csvStream = new StreamWriter(@"C:\Users\emanuele.salvatori.AVANADE-CORP\Desktop\Varie\GiampaoloVacca.csv"))
            {

                csvStream.Write("TeamMembers;");
                for (int i = 1; i<= 31; i++)
                {
                    csvStream.Write(i + ";");
                }
                csvStream.Write("\n");

                foreach(TeamMember teamMember in teamList)
                {
                    csvStream.Write(teamMember.Username + ";");
                    foreach (KeyValuePair<int, List<string>> DailyActivities in teamMember.ActivitiesByDay)
                    {
                        foreach (string DailyActivity in DailyActivities.Value)
                        {

                            csvStream.Write("#"+DailyActivity + ",");
                        }
                        csvStream.Write(";");

                    }
                    csvStream.Write("\n");
                }
           
                csvStream.Flush();
            }
        }




        private static string[] _teamMembers = new string[] { "Antonio Manes", "Emanuele Salvatori", "Gaia Paolini", "Giorgia Loddo", "Luca Stornaiuolo", "Vincenzo Desario" };


        private static bool ValidateServerCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }
        public static List<int> AllIndexesOf(string str, string value)
        {
            if (String.IsNullOrEmpty(value))
                throw new ArgumentException("the string to find may not be empty", "value");
            List<int> indexes = new List<int>();
            for (int index = 0; ; index += value.Length)
            {
                index = str.IndexOf(value, index);
                if (index == -1)
                    return indexes;
                indexes.Add(index);
            }
        }
        private static List<TeamMember> TeamMembersFactory(string[] membersName, List<WorkItemForClient> works)
        {
            List<TeamMember> ret = new List<TeamMember>();
            foreach (string member in membersName)
            {
                List<WorkItemForClient> task = works.Where(work => work.AssignedTo.Contains(member)).ToList();
                TeamMember TeamMember = new TeamMember(task);
                ret.Add(TeamMember);

            }
            return ret;
        }

    }

}
